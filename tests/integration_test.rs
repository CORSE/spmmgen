use num_traits::Float;
use rstest::rstest;
use spmmgen::*;
use std::fmt::Display;

enum ImplemKind {
    Coo,
    Csr,
    Bands,
    BandsWithPermutation,
    Blocks,
    Bourrin,
}

fn cmp_with_naive<T: Float + Display>(
    dense_a: DenseMatrix<T>,
    dense_b: DenseMatrix<T>,
    dense_c: DenseMatrix<T>,
) {
    let len_i = dense_a.nrows;
    let len_j = dense_c.ncols;
    let len_k = dense_b.nrows;
    let epsilon = T::from(0.001).unwrap();
    for i in 0..len_i {
        for j in 0..len_j {
            let mut expected = T::zero();
            for k in 0..len_k {
                expected = expected + dense_a.buf[i * len_k + k] * dense_b.buf[k * len_j + j];
            }
            let get = dense_c.buf[i * len_j + j];
            assert!(
                (expected - get).abs() < epsilon,
                "i={i},j={j} get {get} != expected {expected}"
            );
        }
    }
}

fn generic_test<F: Float + Display, T: TemplateSpmm<F>>(
    dir: &str,
    ukern: MicroKernel,
    spec: T,
    len_j: usize,
    dense_a: DenseMatrix<F>,
    dense_b: DenseMatrix<F>,
    mut dense_c: DenseMatrix<F>,
) {
    println!("Compiling {spec}");
    let lib = CodeGenerator::new()
        .with_root_dir(dir)
        .with_compiler("clang")
        .with_debug()
        .gen_compile_load(&spec, ukern)
        .unwrap();

    unsafe {
        spec.execute_gen_fn(
            &lib,
            len_j as i32,
            dense_b.buf.as_ptr(),
            dense_c.buf.as_mut_ptr(),
        );
    }

    println!("Executing done");
    println!("Comparing...");
    cmp_with_naive(dense_a, dense_b, dense_c);
    println!("Comparing done");
}

#[rstest]
#[case::bands("/tmp/test_spmm1", None, ImplemKind::Bands)]
#[case::bands_tiled("/tmp/test_spmm2", Some(4), ImplemKind::Bands)]
#[case::blocks("/tmp/test_spmm3", None, ImplemKind::Blocks)]
#[case::blocks_tiled("/tmp/test_spmm4", Some(5), ImplemKind::Blocks)]
#[ignore = "Bourrin is too slow to compile"]
#[case::bourrin("/tmp/test_spmm5", None, ImplemKind::Bourrin)]
#[case::coo("/tmp/test_spmm6", None, ImplemKind::Coo)]
#[case::csr("/tmp/test_spmm7", None, ImplemKind::Csr)]
#[case::csr("/tmp/test_spmm8", None, ImplemKind::BandsWithPermutation)]
fn test_exec(#[case] root_dir: &str, #[case] tile: Option<usize>, #[case] kind: ImplemKind) {
    type F = f32;
    let ukern = MicroKernel {
        i: 6,
        j: 2,
        avx: VectorInstructions::Avx2,
    };
    let len_i = ukern.i * 7;
    let jtile = tile.unwrap_or(1);
    let len_j = ukern.j * ukern.avx.vector_size::<F>() * jtile * 3;
    let len_k = 32;
    let dense_a: DenseMatrix<F> = DenseMatrix::filled_no_padding(len_i, len_k);
    let dense_b: DenseMatrix<F> = DenseMatrix::filled_no_padding(len_k, len_j);
    let dense_c: DenseMatrix<F> = DenseMatrix::zeros_no_padding(len_i, len_j);
    let (shape, coo) = dense_a.to_coo();
    match kind {
        ImplemKind::Bands => {
            let b = Bands::new(&dense_a, ukern.i, tile);
            generic_test(root_dir, ukern, b, len_j, dense_a, dense_b, dense_c)
        }
        ImplemKind::BandsWithPermutation => {
            let permut: Vec<usize> = (0..len_i).collect();
            let b = PermutedBands::new(&dense_a, ukern.i, tile, &permut);
            generic_test(root_dir, ukern, b, len_j, dense_a, dense_b, dense_c)
        }
        ImplemKind::Blocks => {
            let b = Blocks::new(&dense_a, ukern.i, len_k / 4, tile);
            generic_test(root_dir, ukern, b, len_j, dense_a, dense_b, dense_c)
        }
        ImplemKind::Bourrin => {
            assert!(len_j > ukern.j * 8);
            println!("J={len_j}");
            let b = Bourrin::from_dense(&dense_a);
            println!("BourrinInfo created");
            generic_test(root_dir, ukern, b, len_j, dense_a, dense_b, dense_c)
        }
        ImplemKind::Coo => {
            println!("COO shape = {shape:?}");
            let b = Coo::new(&coo, tile);
            generic_test(root_dir, ukern, b, len_j, dense_a, dense_b, dense_c)
        }
        ImplemKind::Csr => {
            println!("CSR shape = {shape:?}");
            let b = Csr::new(&coo, shape[0], tile);
            generic_test(root_dir, ukern, b, len_j, dense_a, dense_b, dense_c)
        }
    };
}
