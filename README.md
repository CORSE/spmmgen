# Spmmgen

A library for generating C code and compile it to do sparse x dense matrix
multiplication. It is designed to work with [libloading](https://docs.rs/libloading/latest/libloading/) to load and
execute the specialized code at runtime.

It has been used in this
[other project](https://gitlab.inria.fr/CORSE/sparse_blocking/-/tree/main/) to
test spmm implementations and benchmark them.

Check the tests folder to see examples of how to use it.

## Getting started

Add this line under the `[dependencies]` section in your `Cargo.toml`:

```toml
spmmgen = { version = "0.2", git = "git@gitlab.inria.fr:CORSE/spmmgen.git" }
```

## Documentation

To build and open the library doc just:

```bash
cargo doc --open
```

## Design of the library

The library was designed with the idea of quickly iterating through variations of generated code.
That's why it is based on template C file that are included as string and modified by inserting `#define` and new generated code lines.
It is not a clean method but it is simple for testing variations.

Each rust file in `src/template` is simply a struct implementing the `TemplateSpmm` trait which is responsible for an execute function and a generate_src one that is responsible of the generation.

It is a convenient way to add your own and try it.
In order to play with it you must make it pub in the `src/template/mod.rs` and probably add it to the integration test in `tests/integration_test.rs`. It will test if the implementation is correct and give the same output as a naive dense matmul.

## Notes for versionning

For minor changes that doesn't break user code (e.g don't delete, or change name or signature of a `pub fn`) simply increment the 3rd number in `Cargo.toml`. For breaking changes increment the other ones as you like since the API will change.
