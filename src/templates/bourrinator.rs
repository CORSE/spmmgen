use crate::{utils::TemplateCodeInjector, MicroKernel};
use crate::{DenseMatrix, TemplateSpmm};
use libloading::{Library, Symbol};
use std::fmt::{Display, Write};
use std::{collections::HashMap, error::Error};

use num_traits::Float;

/// A dummy id to try to inline as much as possible values and coordinate of the sparse matrix to
/// the C code and see if the compiler does well.
/// # Important note
/// This approach takes minutes to compile and can segfault gcc.
/// The runtime performance is not impressive.
/// This is why you shouldn't execute it but the idea was interesting.
#[derive(Clone)]
pub struct Bourrin<F: Float + Display> {
    coo: HashMap<usize, Vec<(usize, F)>>, // i:[(k,non_zero)]
}

impl<F: Float + Display> Display for Bourrin<F> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Bourrin")
    }
}

impl<F: Float + Display> Bourrin<F> {
    pub fn from_sparse(triplets: &[(usize, usize, F)]) -> Self {
        let mut coo: HashMap<usize, Vec<(usize, F)>> = HashMap::new();
        for &(i, k, val) in triplets.iter() {
            let k_nz = (k, val);
            coo.entry(i)
                .and_modify(|v| v.push(k_nz))
                .or_insert(vec![k_nz]);
        }

        coo.iter_mut()
            .for_each(|(_, v)| v.sort_by(|a, b| a.0.cmp(&b.0)));
        Bourrin { coo }
    }

    pub fn from_dense(dense: &DenseMatrix<F>) -> Self {
        let mut coo: HashMap<usize, Vec<(usize, F)>> = HashMap::new();
        for i in 0..(dense.pad_nrows) {
            for k in 0..(dense.pad_ncols) {
                let val = dense.buf[i * dense.pad_ncols + k];
                if val.is_zero() {
                    continue;
                }
                let k_nz = (k, val);
                coo.entry(i)
                    .and_modify(|v| v.push(k_nz))
                    .or_insert(vec![k_nz]);
            }
        }
        Bourrin { coo }
    }
}

impl<F: Float + Display> TemplateSpmm<F> for Bourrin<F> {
    fn generate_src(&self, ukern: MicroKernel) -> Result<String, Box<dyn Error>> {
        // 'k' and 'i' will be REPLACED by their harcoded matrix value
        let vectype = ukern.avx.vector_type::<F>();
        let vector_size_float = ukern.avx.vector_size::<F>();
        let broadcast = ukern.avx.broadcast_scal2vec::<F>();
        let load = ukern.avx.load_vec::<F>();
        let store = ukern.avx.store_vec::<F>();
        let fma = ukern.avx.fma_vec::<F>();

        let float_type = match std::mem::size_of::<F>() {
            4 => "float",
            8 => "double",
            other => panic!("Unsupported float size {}", 8 * other),
        };

        let t = "    ";
        let mut buf = String::new();
        for (&i, k_nzs) in self.coo.iter() {
            for (k, nz) in k_nzs.iter() {
                writeln!(
                    buf,
                    "  for (int j=0; j<J; j+={}){{",
                    vector_size_float * ukern.j
                )?;
                writeln!(buf, "{t}{vectype} a_{i}_{k} = {broadcast}({nz});")?;
                for j in 0..ukern.j {
                    let n = vector_size_float * j;
                    writeln!(buf, "{t}{vectype} b_{k}_{j} = {load}(&B[{k} * J + j+{n}]);")?;
                    writeln!(buf, "{t}{vectype} c_{i}_{j} = {load}(&C[{i} * J + j+{n}]);")?;
                    writeln!(
                        buf,
                        "{t}c_{i}_{j} = {fma}(a_{i}_{k}, b_{k}_{j}, c_{i}_{j});"
                    )?;
                    writeln!(buf, "{t}{store}(&C[{i} * J + j+{n}], c_{i}_{j});")?;
                }
                writeln!(buf, "  }}\n")?;
            }
        }

        let code = include_str!("bourrinator.c")
            .define("FLOAT", float_type)
            .define("VECSIZE", vector_size_float)
            .insert_code("BOURRIN", &buf);
        Ok(code)
    }

    type GenFn = extern "C" fn(i32, *const F, *mut F);

    unsafe fn execute_gen_fn(&self, lib: &Library, len_j: i32, b: *const F, c: *mut F) {
        let implem: Symbol<Self::GenFn> = lib.get(b"gen_spmm_bourrin").unwrap();

        (implem)(len_j, b, c)
    }
}
