use crate::utils::TemplateCodeInjector;
use crate::{MicroKernel, TemplateSpmm};
use libloading::{Library, Symbol};
use num_traits::Float;
use std::error::Error;
use std::{fmt::Display, iter::repeat};

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct CsrVal<T: Float> {
    k: i32,
    val: T,
}

/// Usual Csr data but with col and values merged in one array of struct since they are accessed
/// together.
#[derive(Clone)]
pub struct Csr<F: Float + Display> {
    /// number of rows
    len_i: usize,

    /// Pairs of active k and nonzero value.
    /// Length of this vector is the number of nonzeros (nnz).
    pub nonzeros_col: Vec<CsrVal<F>>,

    /// CSR cummulative sum of nnz per row.
    /// Length of this vector is the number of rows `len_i`.
    pub ptr_end: Vec<i32>,
    tile_j: Option<usize>,
}

impl<F: Float + Display> Display for Csr<F> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "CSR")
    }
}

impl<F: Float + Display> Csr<F> {
    /// **THIS FUNCTION EXPECT COO INPUT TO BE SORTED**
    pub fn new(triplets: &[(usize, usize, F)], len_i: usize, tile_j: Option<usize>) -> Self {
        // assert!(indices.is_sorted());
        let nnz = triplets.len();
        let mut nonzeros_col: Vec<CsrVal<F>> = Vec::with_capacity(nnz);
        let mut ptr_end: Vec<i32> = Vec::with_capacity(len_i);
        let mut cur_i = 0;
        let mut cur_nnz = 0;
        // convert coo to csr
        // count how much nonzeros on the same line and group them
        for &(i, k, val) in triplets.iter() {
            let k = k as i32;
            nonzeros_col.push(CsrVal { k, val });
            if cur_i < i {
                // new row so commit the previous one i2 - i1 times
                // since CSR got len_i element in ptr array we duplicate the same nnz
                ptr_end.extend(repeat(cur_nnz).take(i - cur_i));
                cur_i = i;
            }
            // we should have cur_i == i
            cur_nnz += 1;
        }
        assert_eq!(cur_nnz, nnz as i32);
        ptr_end.push(nnz as i32);
        assert_eq!(ptr_end.len(), len_i);

        Csr {
            tile_j,
            len_i,
            ptr_end,
            nonzeros_col,
        }
    }
}

impl<F: Float + Display> TemplateSpmm<F> for Csr<F> {
    fn generate_src(&self, mut ukern: MicroKernel) -> Result<String, Box<dyn Error>> {
        ukern.i = 1;
        let vecs = ukern.declare_vecs::<F>(1)?;
        let tile_j = self.tile_j.unwrap_or(0);
        let (tiling_loop_start, tiling_loop_end) = ukern.tiling_j_loop(tile_j);
        let vector_size_float = ukern.avx.vector_size::<F>();
        let kernel_src = ukern.gen_code::<F>(3, "val", "k * J + (j + unroll_j)")?;
        let loads = ukern.gen_loads_c::<F>("i * J + (j + unroll_j)", 2)?;
        let stores = ukern.gen_store_c::<F>("i * J + (j + unroll_j)", 2)?;
        let float_type = match std::mem::size_of::<F>() {
            4 => "float",
            8 => "double",
            other => panic!("Unsupported float size {}", 8 * other),
        };

        let code = include_str!("csr.c")
            .define("FLOAT", float_type)
            .define("VECSIZE", vector_size_float)
            .define("I", self.len_i)
            .define("J_TILE", tile_j)
            .define("UKERNJ", ukern.j)
            .define("NNZ", self.nonzeros_col.len())
            .insert_code("DECLARATIONS", &vecs)
            .insert_code("START OPTIONNAL TILING J", &tiling_loop_start)
            .insert_code("LOADS C", &loads)
            .insert_code("KERNEL", &kernel_src)
            .insert_code("STORES C", &stores)
            .insert_code("END OPTIONNAL TILING J", &tiling_loop_end);
        Ok(code)
    }

    type GenFn = extern "C" fn(i32, *const F, *mut F, *const CsrVal<F>, *const i32);

    unsafe fn execute_gen_fn(&self, lib: &Library, len_j: i32, b: *const F, c: *mut F) {
        let implem: Symbol<Self::GenFn> = lib.get(b"gen_spmm_csr").unwrap();

        (implem)(
            len_j,
            b,
            c,
            self.nonzeros_col.as_ptr(),
            self.ptr_end.as_ptr(),
        )
    }
}
