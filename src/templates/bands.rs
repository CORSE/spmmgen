use std::error::Error;
use std::fmt::Debug;
use std::fmt::Display;
use std::fmt::Write;

use crate::utils::TemplateCodeInjector;
use crate::TemplateSpmm;
use crate::{dense::DenseMatrix, utils::IntPair, MicroKernel};
use libloading::{Library, Symbol};
use num_traits::Float;

/// Sparse format for the Band implementation contains also some data for the generated code.
#[derive(Default, Clone, Debug)]
pub struct Bands<F: Float + Display> {
    /// The size of every horizontal band.
    pub height: usize,

    /// Pais of int such as pair.i is the active row i and  
    /// pair.j is the number of actives columns k in this row
    pub offsets_i_lens_k: Vec<IntPair>,

    pub actives_k: Vec<i32>,
    /// Non zeros elements, it possibly contains some zeros since Band do zerofilling to complete
    /// the dense vertical blocks of size `height`x1.
    pub values: Vec<F>,

    /// Should we tile on j and how many if we should (multiple of kernel.j)
    /// i.e tile_j=Some(3) means that every tile will have size of 3*kernel.j.
    pub tile_j: Option<usize>,
}

impl<F: Float + Display> Display for Bands<F> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Bands")
    }
}

impl<F: Float + Display> Bands<F> {
    /// Constructor from a [DenseMatrix] and the size of band `bi` and the desired tiling on j.
    pub fn new(dense: &DenseMatrix<F>, bi: usize, tile_j: Option<usize>) -> Self {
        assert!(bi > 0);
        let len_i = dense.pad_nrows;
        let len_k = dense.pad_ncols;
        assert!(len_i % bi == 0);
        // be careful actives_bands as len of len_i / bi
        // so when converting to index in the original matrix we need to multiply by bi
        let actives_bands: Vec<Vec<usize>> = (0..len_i)
            .step_by(bi)
            .map(|i| {
                (0..len_k)
                    .filter_map(|k| {
                        let offset = i * len_k + k;
                        if (0..bi).any(|i| !dense.buf[offset + i * len_k].is_zero()) {
                            Some(k)
                        } else {
                            None
                        }
                    })
                    .collect()
            })
            .collect();

        let mut actives_k: Vec<i32> = actives_bands
            .iter()
            .flat_map(|k| k.iter().map(|x| *x as i32))
            .collect();
        // add an extra element just to avoid the prefetch k
        // to read 1 element outside the array
        actives_k.push(*actives_k.last().unwrap());

        Bands {
            offsets_i_lens_k: actives_bands
                .iter()
                .enumerate()
                .filter(|(_, v)| !v.is_empty())
                .map(|(i, actives_k_band)| IntPair {
                    i: (i * bi) as i32, // dont forget to multiply by bi
                    j: actives_k_band.len() as i32,
                })
                .collect(),
            actives_k,
            values: actives_bands
                .into_iter()
                .enumerate()
                .filter(|(_, v)| !v.is_empty())
                .flat_map(|(i, actives_k_band)| {
                    actives_k_band.into_iter().flat_map(move |k| {
                        // here also the index i is in 0..(len_i/bi) and need to be multiply by bi
                        (0..bi).map(move |idx| dense.buf[(i * bi + idx) * len_k + k])
                    })
                })
                .collect(),
            height: bi,
            tile_j,
        }
    }
}

impl<F: Float + Display> TemplateSpmm<F> for Bands<F> {
    fn generate_src(&self, ukern: MicroKernel) -> Result<String, Box<dyn Error>> {
        let access_a = "A[kb * BI + unroll_i]";
        let access_b = "k * J + (j + unroll_j)";
        let access_c = "(i + unroll_i) * J + (j + unroll_j)";

        let inner_level = 5;
        let outter_level = 1;
        let vecs = ukern.declare_vecs::<F>(outter_level)?;
        let tile_j = self.tile_j.unwrap_or(0);
        let float_type = match std::mem::size_of::<F>() {
            4 => "float",
            8 => "double",
            other => panic!("Unsupported float size {}", 8 * other),
        };

        let vector_size_float = ukern.avx.vector_size::<F>();

        let mut core_prefetch = String::new();
        for j in 1..ukern.j {
            let offset = j * vector_size_float;
            writeln!(
                core_prefetch,
                "        _mm_prefetch(&B[k_prefetch * J + j + {offset}], _MM_HINT_T0);"
            )?;
        }

        let loadc = ukern.gen_loads_c::<F>(access_c, inner_level - 1)?;
        let storec = ukern.gen_store_c::<F>(access_c, inner_level - 1)?;

        let ukern_code = ukern.gen_code::<F>(inner_level, access_a, access_b)?;
        let (tiling_loop_start, tiling_loop_end) = ukern.tiling_j_loop(tile_j);

        let code = include_str!("bands.c")
            .define("FLOAT", float_type)
            .define("VECSIZE", vector_size_float)
            .define("J_UKERN", ukern.j)
            .define("I_UKERN", ukern.i)
            .define("J_TILE", tile_j)
            .define("NBANDS", self.offsets_i_lens_k.len())
            .insert_code("DECLARATIONS", &vecs)
            .insert_code("START OPTIONNAL TILING J", &tiling_loop_start)
            .insert_code("LOADS C", &loadc)
            .insert_code("PREFETCH", &core_prefetch)
            .insert_code("KERNEL", &ukern_code)
            .insert_code("STORES C", &storec)
            .insert_code("END OPTIONNAL TILING J", &tiling_loop_end);
        Ok(code)
    }

    type GenFn = extern "C" fn(i32, *const F, *mut F, *const IntPair, *const i32, *const F);

    unsafe fn execute_gen_fn(&self, lib: &Library, len_j: i32, b: *const F, c: *mut F) {
        let implem: Symbol<Self::GenFn> = lib.get(b"gen_spmm_bands").unwrap();

        (implem)(
            len_j,
            b,
            c,
            self.offsets_i_lens_k.as_ptr(),
            self.actives_k.as_ptr(),
            self.values.as_ptr(),
        )
    }
}

#[cfg(test)]
mod test_band {
    use super::*;

    #[test]
    fn test_band_len1() {
        let n = 12;
        let coo: Vec<(usize, usize, f32)> = (0..n).map(|i| (i, i, 42.0)).collect();
        let dense = DenseMatrix::from_coo([n, n], &coo);
        let band = Bands::new(&dense, 1, None);
        assert_eq!(
            band.values.len(),
            coo.len(),
            "Band of height 1 and COO should have same nnz (no zerofilling)"
        );
    }
}
