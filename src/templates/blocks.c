#include <assert.h>
#include <x86intrin.h>

// INSERT DEFINES

typedef struct intpair {
  int i;
  int j;
} intpair;

void gen_spmm_blocks(const int J, FLOAT const *const __restrict__ B,
                     FLOAT *const __restrict__ C,
                     const intpair *const blocks_indices,
                     const FLOAT *const __restrict__ blocks_values) {

  assert(J > 0);
  const int ukern_j_size = J_UKERN * VECSIZE;
  assert(J % ukern_j_size == 0);
  const int BI = I_UKERN;
  const int BLOCSIZE = BI * BK;
  // INSERT DECLARATIONS
  int jstart = 0; // inner tile j start
  int jend = J;   // inner tile j end
  // INSERT START OPTIONNAL TILING J
  for (int idx = 0; idx < NBLOCKS; ++idx) {
    const intpair tmp = blocks_indices[idx];
    const int i = tmp.i;
    const int kmin = tmp.j;
    const FLOAT *const A = blocks_values + idx * BLOCSIZE;
    for (int j = jstart; j < jend; j += ukern_j_size) {
      // INSERT LOADS C
      for (int kb = 0; kb < BK; ++kb) {
        int k = kmin + kb;
        // INSERT KERNEL
      }
      // INSERT STORES C
    }
  }
  // INSERT END OPTIONNAL TILING J
}
