#include <assert.h>
#include <x86intrin.h>

// INSERT DEFINES

typedef struct triplet {
  int i;
  int k;
  FLOAT nonzero;
} triplet;

void gen_spmm_coo(const int J, const FLOAT *const __restrict__ B,
                  FLOAT *__restrict__ C, const triplet *const A) {
  assert(J % VECSIZE == 0);
  int jstart = 0;
  int jend = J;
  // INSERT DECLARATIONS
  // INSERT START OPTIONNAL TILING J
  for (int idx = 0; idx < NNZ; idx++) {
    triplet t = A[idx];
    int i = t.i;
    int k = t.k;
    FLOAT val = t.nonzero;
    for (int j = jstart; j < jend; j += (UKERNJ * VECSIZE)) {
      // INSERT LOADS C
      // INSERT KERNEL
      // INSERT STORES C
    }
  }
  // INSERT END OPTIONNAL TILING J
}
