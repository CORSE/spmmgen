#include <assert.h>
#include <x86intrin.h>

// INSERT DEFINES

typedef struct CsrVal {
  int k;
  FLOAT val;
} CsrVal;

void gen_spmm_csr(const int J, const FLOAT *const __restrict__ B,
                  FLOAT *__restrict__ C, const CsrVal *const values,
                  const int *const ptr_end) {
  assert(J % VECSIZE == 0);
  int jstart = 0;
  int jend = J;
  int offset = 0;
  // INSERT DECLARATIONS
  // INSERT START OPTIONNAL TILING J
  for (int i = 0; i < I; i++) {
    for (int idx_o = offset; idx_o < ptr_end[i]; idx_o++) {
      CsrVal tmp = values[idx_o];
      int k = tmp.k;
      FLOAT val = tmp.val;
      for (int j = jstart; j < jend; j += (UKERNJ * VECSIZE)) {
        // INSERT LOADS C
        // INSERT KERNEL
        // INSERT STORES C
      }
    }
    offset = ptr_end[i];
  }
  // INSERT END OPTIONNAL TILING J
}
