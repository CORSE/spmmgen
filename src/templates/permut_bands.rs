use std::error::Error;
use std::fmt::Debug;
use std::fmt::Display;

use crate::dense::DenseMatrix;
use crate::utils::IntPair;
use crate::utils::TemplateCodeInjector;
use crate::Bands;
use crate::MicroKernel;
use crate::TemplateSpmm;
use libloading::{Library, Symbol};
use num_traits::Float;

/// This is a variation of [Bands] but using a permutation of rows.
/// The permutation could in some cases improve locality and cache behavior.
/// This is the case with some matrices like "c8_mat11" from [SuiteSparse](https://sparse.tamu.edu/).
#[derive(Default, Clone, Debug)]
pub struct PermutedBands<F: Float + Display> {
    /// The permuted band data.
    pub band: Bands<F>,

    /// The permutation vector useful for reverse the permutation.
    /// It is used by the generated code to still be a valid `A *
    /// B = C` operator.
    /// Indeed we are computing a permutation of `A` and computing`A2 * B = C2`.
    /// For still computing `C` we must apply a permutation on the rows of `C2`.
    /// This permutation must reverse the old permutation and get `C`.
    pub permut_rows: Vec<i32>,
}

impl<F: Float + Display> PermutedBands<F> {
    /// Hypothesis input is not already permuted
    pub fn new(
        dense: &DenseMatrix<F>,
        bi: usize,
        tile_j: Option<usize>,
        permut_vec: &[usize],
    ) -> Self {
        let permuted_dense = dense.permute_rows(permut_vec);

        Self {
            band: Bands::new(&permuted_dense, bi, tile_j),
            permut_rows: permut_vec.iter().map(|&i| i as i32).collect(),
        }
    }
}
impl<F: Float + Display> Display for PermutedBands<F> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "BandsWithPermutations")
    }
}

impl<F: Float + Display> TemplateSpmm<F> for PermutedBands<F> {
    fn generate_src(&self, ukern: MicroKernel) -> Result<String, Box<dyn Error>> {
        let code = self
            .band
            .generate_src(ukern)?
            .define("I", self.permut_rows.len())
            .define("PERMUTATION_ON", 1);
        Ok(code)
    }

    type GenFn =
        extern "C" fn(i32, *const F, *mut F, *const i32, *const IntPair, *const i32, *const F);

    unsafe fn execute_gen_fn(&self, lib: &Library, len_j: i32, b: *const F, c: *mut F) {
        let implem: Symbol<Self::GenFn> = lib.get(b"gen_spmm_bands_permuted").unwrap();
        (implem)(
            len_j,
            b,
            c,
            self.permut_rows.as_ptr(),
            self.band.offsets_i_lens_k.as_ptr(),
            self.band.actives_k.as_ptr(),
            self.band.values.as_ptr(),
        )
    }
}
