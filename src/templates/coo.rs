use crate::utils::TemplateCodeInjector;
use crate::{MicroKernel, TemplateSpmm};

use libloading::{Library, Symbol};
use num_traits::Float;
use std::error::Error;
use std::fmt::Display;

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Triplet<T: Float> {
    i: i32,
    k: i32,
    val: T,
}

/// Usual COO data, simple list of triplets containings 2 indices for the coordinate
/// an one value of the nonzero.
#[derive(Clone)]
pub struct Coo<F: Float + Display> {
    pub triplets: Vec<Triplet<F>>,
    tile_j: Option<usize>,
}

impl<F: Float + Display> Display for Coo<F> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "COO")
    }
}

impl<F: Float + Display> Coo<F> {
    pub fn new(triplets: &[(usize, usize, F)], tile_j: Option<usize>) -> Self {
        Coo {
            tile_j,
            triplets: triplets
                .iter()
                .map(|&(i, k, val)| Triplet {
                    i: i as i32,
                    k: k as i32,
                    val,
                })
                .collect(),
        }
    }
}

impl<F: Float + Display> TemplateSpmm<F> for Coo<F> {
    fn generate_src(&self, mut ukern: MicroKernel) -> Result<String, Box<dyn Error>> {
        ukern.i = 1;
        let vecs = ukern.declare_vecs::<F>(1)?;
        let tile_j = self.tile_j.unwrap_or(0);
        let (tiling_loop_start, tiling_loop_end) = ukern.tiling_j_loop(tile_j);
        let vector_size_float = ukern.avx.vector_size::<F>();
        let kernel_src = ukern.gen_code::<F>(3, "val", "k * J + (j + unroll_j)")?;
        let loads = ukern.gen_loads_c::<F>("i * J + (j + unroll_j)", 2)?;
        let stores = ukern.gen_store_c::<F>("i * J + (j + unroll_j)", 2)?;
        let float_type = match std::mem::size_of::<F>() {
            4 => "float",
            8 => "double",
            other => panic!("Unsupported float size {}", 8 * other),
        };

        let code = include_str!("coo.c")
            .define("FLOAT", float_type)
            .define("VECSIZE", vector_size_float)
            .define("UKERNJ", ukern.j)
            .define("J_TILE", tile_j)
            .define("NNZ", self.triplets.len())
            .insert_code("DECLARATIONS", &vecs)
            .insert_code("START OPTIONNAL TILING J", &tiling_loop_start)
            .insert_code("LOADS C", &loads)
            .insert_code("KERNEL", &kernel_src)
            .insert_code("STORES C", &stores)
            .insert_code("END OPTIONNAL TILING J", &tiling_loop_end);
        Ok(code)
    }

    type GenFn = extern "C" fn(i32, *const F, *mut F, *const Triplet<F>);

    unsafe fn execute_gen_fn(&self, lib: &Library, len_j: i32, b: *const F, c: *mut F) {
        let implem: Symbol<Self::GenFn> = lib.get(b"gen_spmm_coo").unwrap();

        (implem)(len_j, b, c, self.triplets.as_ptr())
    }
}
