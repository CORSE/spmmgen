#include <assert.h>
#include <string.h>
#include <x86intrin.h>
#include <xmmintrin.h>

// INSERT DEFINES

typedef struct intpair {
  int i;
  int j;
} intpair;

#ifdef PERMUTATION_ON
void gen_spmm_bands_permuted(const int J, FLOAT const *const __restrict__ B,
                             FLOAT *const __restrict__ C_out,
                             const int *const __restrict__ permutation,
                             const intpair *const bands_info,
                             const int *const __restrict__ actives_k,
                             const FLOAT *const __restrict__ values) {
#else
void gen_spmm_bands(const int J, FLOAT const *const __restrict__ B,
                    FLOAT *const __restrict__ C,
                    const intpair *const bands_info, const int *const actives_k,
                    const FLOAT *const __restrict__ values) {
#endif /* ifdef PERMUTATION_ON */

  assert(J > 0);
  const int ukern_j_size = J_UKERN * VECSIZE;
  assert(J % ukern_j_size == 0);
  const int BI = I_UKERN;
#ifdef PERMUTATION_ON
  FLOAT *const C = calloc(J * I, sizeof(FLOAT));
#endif /* ifdef PERMUTATION_ON  */
  // INSERT DECLARATIONS
  int jstart = 0;
  int jend = J;
  // INSERT START OPTIONNAL TILING J
  int const *actives_cols = actives_k;
  const FLOAT *A = values;
  for (int idx = 0; idx < NBANDS; ++idx) {
    const intpair tmp = bands_info[idx];
    const int i = tmp.i;
    const int n_actives_k = tmp.j;
    for (int j = jstart; j < jend; j += ukern_j_size) {
      // INSERT LOADS C

      int k_prefetch = actives_cols[0];
      _mm_prefetch(&B[k_prefetch * J + j], _MM_HINT_T0);
      for (int kb = 0; kb < n_actives_k; ++kb) {
        int k = k_prefetch;
        k_prefetch = actives_cols[kb + 1];
        _mm_prefetch(&B[k_prefetch * J + j], _MM_HINT_T0);
        // INSERT PREFETCH

        // INSERT KERNEL
      }
      // INSERT STORES C
    }
    A += BI * n_actives_k;
    actives_cols += n_actives_k;
  }
  // INSERT END OPTIONNAL TILING J

#ifdef PERMUTATION_ON
  const size_t len_row = J * sizeof(FLOAT);
  for (int new_i = 0; new_i < I; new_i++) {
    int original_i = permutation[new_i];
    memcpy(C_out + (original_i * J), C + (new_i * J), len_row);
  }
  free(C);
#endif /* ifndef PERMUTATION_ON */
}
