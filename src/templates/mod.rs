//! This module is the place to add new SpMM implementations to test.
//! All main struct from this module implements the [TemplateSpmm] trait
//! in order to be used generically by a [CodeGenerator](crate::CodeGenerator).
mod bands;
mod blocks;
mod bourrinator;
mod coo;
mod csr;
mod permut_bands;

pub use bands::Bands;
pub use blocks::Blocks;
pub use bourrinator::Bourrin;
pub use coo::Coo;
pub use csr::Csr;
pub use permut_bands::PermutedBands;

use crate::MicroKernel;
use libloading::Library;
use num_traits::Float;
use std::error::Error;
use std::fmt::Display;

/// The trait that every codegen implementation should conform to.
pub trait TemplateSpmm<F: Float + Display>: Display {
    /// The signature of the extern C function that your implementation is using.
    type GenFn;
    /// A way to generate the C source of the codegen implementation as a [String].
    fn generate_src(&self, ukern: MicroKernel) -> Result<String, Box<dyn Error>>;
    /// A way to execute the generate function that must have been compiled and loaded.
    /// # Error
    /// It will panic if the given library doesn't hold the function needed.
    /// If a symbol match but doesn't do what you want (example not the same signature)
    /// it is undefined behavior.
    unsafe fn execute_gen_fn(&self, lib: &Library, len_j: i32, b: *const F, c: *mut F);
}
