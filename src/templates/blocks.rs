use crate::utils::TemplateCodeInjector;
use crate::TemplateSpmm;
use std::error::Error;
use std::fmt::{Debug, Display};

use libloading::{Library, Symbol};

use crate::utils::IntPair;
use crate::{dense::DenseMatrix, MicroKernel};
use num_traits::Float;

/// Sparse format for blocks.
#[derive(Clone, Debug)]
pub struct Blocks<F: Float + Display> {
    /// number of blocks (should be equal to values.len())
    pub nblocs: usize,

    /// Number of rows in the blocks i.e vertical dimension of the block.
    pub blocsize_rows: usize,

    /// Number of columns in the blocks i.e horizontal dimension of the block.
    pub blocsize_cols: usize,

    /// Coordinates in the original matrix of the i,k of the top left of each block.
    /// They are ordered in a left to right, top to bottom way i.e lexicographic (i,k).
    pub indices: Vec<IntPair>,

    /// Flattened and zero filled non-zero elements of each block.
    /// They are ordered in a left to right, top to bottom way (same as indices).
    /// Each index in `indices` is mapped to `blocsize_rows * blocsize_cols` float in `values`.
    pub values: Vec<F>,

    /// Should we tile on j and how many if we should (multiple of kernel.j)
    /// i.e tile_j=Some(3) means that every tile will have size of 3*kernel.j.
    pub tile_j: Option<usize>,
}

impl<F: Float + Display> Display for Blocks<F> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Blocks")
    }
}

fn is_block_occupied<F: Float>(
    offset: usize,
    dense: &[F],
    bi: usize,
    bj: usize,
    len_j: usize,
) -> bool {
    for i in 0..bi {
        for j in 0..bj {
            if !dense[offset + i * len_j + j].is_zero() {
                return true;
            }
        }
    }
    false
}

fn zeros_filling<F: Float>(
    mut offset: usize,
    values: &mut Vec<F>,
    bi: usize,
    bj: usize,
    len_j: usize,
    dense: &[F],
) {
    for _ in 0..bi {
        values.extend(&dense[offset..(offset + bj)]);
        offset += len_j;
    }
}

impl<F: Float + Display> Blocks<F> {
    /// # Arguments
    /// - `bi`: same as `blocsize_rows`.
    /// - `bj`: same as `blocsize_cols`.
    /// - `tile_j`: same as `tile_j`.
    pub fn new(dense: &DenseMatrix<F>, bi: usize, bj: usize, tile_j: Option<usize>) -> Self {
        let len_i = dense.pad_nrows;
        let len_j = dense.pad_ncols;
        let mut nblocs = 0;
        for i in (0..len_i).step_by(bi) {
            for j in (0..len_j).step_by(bj) {
                if is_block_occupied(i * len_j + j, &dense.buf, bi, bj, len_j) {
                    nblocs += 1;
                }
            }
        }
        let mut indices: Vec<IntPair> = Vec::with_capacity(nblocs);
        let mut values: Vec<F> = Vec::with_capacity(nblocs * bi * bj);
        for i in (0..len_i).step_by(bi) {
            for j in (0..len_j).step_by(bj) {
                if is_block_occupied(i * len_j + j, &dense.buf, bi, bj, len_j) {
                    zeros_filling(i * len_j + j, &mut values, bi, bj, len_j, &dense.buf);
                    indices.push(IntPair {
                        i: i as i32,
                        j: j as i32,
                    });
                }
            }
        }

        Blocks {
            nblocs,
            blocsize_rows: bi,
            blocsize_cols: bj,
            indices,
            values,
            tile_j,
        }
    }
}

impl<F: Float + Display> TemplateSpmm<F> for Blocks<F> {
    fn generate_src(&self, ukern: MicroKernel) -> Result<String, Box<dyn Error>> {
        let access_c = "(i + unroll_i) * J + (j + unroll_j)";
        let access_b = "k * J + (j + unroll_j)";
        let access_a = "A[unroll_i * BK + kb]";
        let inner_level = 5;
        let outter_level = 1;
        let vecs = ukern.declare_vecs::<F>(outter_level)?;
        let tile_j = self.tile_j.unwrap_or(0);
        let float_type = match std::mem::size_of::<F>() {
            4 => "float",
            8 => "double",
            other => panic!("Unsupported float size {}", 8 * other),
        };

        let vector_size_float = ukern.avx.vector_size::<F>();

        let loadc = ukern.gen_loads_c::<F>(access_c, inner_level - 1)?;
        let storec = ukern.gen_store_c::<F>(access_c, inner_level - 1)?;

        let ukern_code = ukern.gen_code::<F>(inner_level, access_a, access_b)?;
        let (tiling_loop_start, tiling_loop_end) = ukern.tiling_j_loop(tile_j);

        let code = include_str!("blocks.c")
            .define("FLOAT", float_type)
            .define("VECSIZE", vector_size_float)
            .define("J_UKERN", ukern.j)
            .define("I_UKERN", ukern.i)
            .define("J_TILE", tile_j)
            .define("BK", self.blocsize_cols)
            .define("NBLOCKS", self.nblocs)
            .insert_code("DECLARATIONS", &vecs)
            .insert_code("START OPTIONNAL TILING J", &tiling_loop_start)
            .insert_code("LOADS C", &loadc)
            .insert_code("KERNEL", &ukern_code)
            .insert_code("STORES C", &storec)
            .insert_code("END OPTIONNAL TILING J", &tiling_loop_end);

        Ok(code)
    }

    type GenFn = extern "C" fn(i32, *const F, *mut F, *const IntPair, *const F);

    unsafe fn execute_gen_fn(&self, lib: &Library, len_j: i32, b: *const F, c: *mut F) {
        let implem: Symbol<Self::GenFn> = lib.get(b"gen_spmm_blocks").unwrap();
        (implem)(len_j, b, c, self.indices.as_ptr(), self.values.as_ptr())
    }
}
