//! This library generate optimized C code for sparse x dense matrix multiply (SpMM).
//! It contains some common implementation like [CSR](crate::Csr) and [COO](crate::Coo) and other more custom strategies.
//! Among these alternative strategies we must note [Bands] and [Blocks] which try to exploit
//! unrolled dense [MicroKernel].
//! # Usage
//! ```rust
//! use spmmgen::*;
//!
//! fn main() {
//!     let n = 8;
//!     let b:Vec<f32> =  (0..(n * n)).map(|i| i as f32).collect();
//!     let mut c:Vec<f32> =  vec![0.0;n*n];
//!     // fill matrix a with some nonzero elements
//!     let triplets:[(usize,usize,f32);3] = [(1,1,1.0), (2,3,2.0), (4,5,3.0)];
//!     let coo = Coo::new(&triplets, None);
//!     let kernel = MicroKernel{i:1, j:1, avx:VectorInstructions::Avx2};
//!     let lib = CodeGenerator::new()
//!         .gen_compile_load(&coo, kernel)
//!         .unwrap();
//!     unsafe {
//!         coo.execute_gen_fn(
//!             &lib,
//!             n as i32,
//!             b.as_ptr(),
//!             c.as_mut_ptr()
//!         );
//!     }
//! }
//! ```
mod code_generator;
mod dense;
mod microkernel;
pub mod templates;
mod utils;

pub use code_generator::CodeGenerator;
pub use dense::DenseMatrix;
pub use microkernel::MicroKernel;
pub use microkernel::VectorInstructions;

pub use templates::Bands;
pub use templates::Blocks;
pub use templates::Bourrin;
pub use templates::Coo;
pub use templates::Csr;
pub use templates::PermutedBands;
pub use templates::TemplateSpmm;
