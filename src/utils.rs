use std::fmt::Display;

/// Utility trait to add code and define values in C code in String
pub trait TemplateCodeInjector {
    fn define<T: Display>(self, name: &str, value: T) -> String;
    fn insert_code(self, section: &str, code: &str) -> String;
}

const DEF_INS: &str = "// INSERT DEFINES\n";
impl TemplateCodeInjector for String {
    fn define<T: Display>(mut self, name: &str, value: T) -> String {
        let index = self.find(DEF_INS).unwrap() + DEF_INS.len() + 1;
        self.insert_str(index, &format!("#define {name} {value}\n"));
        self
    }

    fn insert_code(mut self, section: &str, code: &str) -> String {
        let pattern = format!("// INSERT {section}\n");
        let pos = self.find(pattern.as_str()).unwrap();
        self.insert_str(pos + pattern.len(), code);
        self
    }
}

impl TemplateCodeInjector for &str {
    fn define<T: Display>(self, name: &str, value: T) -> String {
        self.to_owned().define(name, value)
    }

    fn insert_code(self, section: &str, code: &str) -> String {
        self.to_owned().insert_code(section, code)
    }
}

/// Useful struct for passing pair of int to generated C code.
#[repr(C)]
#[derive(Debug, Clone, Copy, Default, PartialEq, Eq)]
pub struct IntPair {
    pub i: i32,
    pub j: i32,
}

impl Display for IntPair {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{{{}, {}}}", self.i, self.j)
    }
}
