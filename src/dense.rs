use num_traits::Float;

/// Utility struct to manage the dense matrices B and C in SpMM.
/// In particular it can enable some **padding** which is really convenient for experimenting with the
/// library on buffer of **length non muliple of AVX vector size**.
/// It also enable some easier conversion between the various sparse formats.
#[derive(Debug, Clone, Default, PartialEq, Eq)]
pub struct DenseMatrix<T: Float> {
    pub buf: Vec<T>,
    pub nrows: usize,
    pub ncols: usize,
    pub pad_nrows: usize,
    pub pad_ncols: usize,
}

impl<T: Float> DenseMatrix<T> {
    /// Initialize a buffer filled with zeros and padding.
    pub fn zeros_padded(nrows: usize, ncols: usize, pad_nrows: usize, pad_ncols: usize) -> Self {
        DenseMatrix {
            buf: vec![T::zero(); pad_nrows * pad_ncols],
            nrows,
            ncols,
            pad_nrows,
            pad_ncols,
        }
    }
    /// Initialize a buffer filled with zeros and no padding.
    pub fn zeros_no_padding(nrows: usize, ncols: usize) -> Self {
        DenseMatrix {
            buf: vec![T::zero(); nrows * ncols],
            nrows,
            ncols,
            pad_nrows: nrows,
            pad_ncols: ncols,
        }
    }
    /// Initialize a [DenseMatrix] with an already existing buffer and number of rows.
    /// It doesn't pad and will derive the number of cols based on buffer length.
    pub fn new_no_padding(buf: Vec<T>, nrows: usize) -> Self {
        DenseMatrix {
            nrows,
            ncols: buf.len() / nrows,
            pad_nrows: nrows,
            pad_ncols: buf.len() / nrows,
            buf,
        }
    }

    /// Wrapper arround raw struct initialization with some assertions on lengths with respect to
    /// padding.
    /// The buffer is supposed to have size that match padded lengths and to have logical number of
    /// rows an cols less than their padded equivalents.
    pub fn new_padding(
        buf: Vec<T>,
        nrows: usize,
        ncols: usize,
        pad_nrows: usize,
        pad_ncols: usize,
    ) -> Self {
        assert_eq!(
            buf.len(),
            pad_nrows * pad_ncols,
            "Len of buf {} should match padded sizes {pad_nrows} * {pad_ncols}",
            buf.len()
        );
        assert!(nrows <= pad_nrows);
        assert!(ncols <= pad_ncols);

        DenseMatrix {
            nrows,
            ncols,
            pad_nrows,
            pad_ncols,
            buf,
        }
    }
    /// Initialize buffer with numbers in `0..10` without any padding.
    pub fn filled_no_padding(nrows: usize, ncols: usize) -> Self {
        DenseMatrix {
            buf: (0..(nrows * ncols))
                .map(|x| T::from(x % 10).unwrap())
                .collect(),
            nrows,
            ncols,
            pad_nrows: nrows,
            pad_ncols: ncols,
        }
    }

    /// Initialize buffer with numbers in `0..10` and take care of padding.
    pub fn filled_padding(nrows: usize, ncols: usize, pad_nrows: usize, pad_ncols: usize) -> Self {
        assert!(nrows <= pad_nrows);
        assert!(ncols <= pad_ncols);
        DenseMatrix {
            buf: (0..(pad_nrows * pad_ncols))
                .map(|x| T::from(x % 10).unwrap())
                .collect(),
            nrows,
            ncols,
            pad_nrows,
            pad_ncols,
        }
    }

    /// Helper method to convert [DenseMatrix] to a COO sparse format.
    pub fn to_coo(&self) -> ([usize; 2], Vec<(usize, usize, T)>) {
        let n = self.pad_ncols;
        let shape = [self.nrows, self.ncols];
        let triplets: Vec<(usize, usize, T)> = (0..(self.nrows))
            .flat_map(|i| (0..(self.ncols)).map(move |j| (i, j, self.buf[i * n + j])))
            .filter(|(_, _, val)| !val.is_zero())
            .collect();
        (shape, triplets)
    }

    /// Helper method to create a [DenseMatrix] from COO sparse format.
    pub fn from_coo(shape: [usize; 2], triplets: &[(usize, usize, T)]) -> Self {
        let mut buf = vec![T::zero(); shape[0] * shape[1]];
        triplets.iter().for_each(|&(i, k, val)| {
            buf[i * shape[1] + k] = val;
        });
        DenseMatrix {
            buf,
            nrows: shape[0],
            ncols: shape[1],
            pad_nrows: shape[0],
            pad_ncols: shape[1],
        }
    }
    /// Helper method to create a [DenseMatrix] from COO sparse format but with care of padding.
    pub fn from_coo_padded(
        shape: [usize; 2],
        triplets: &[(usize, usize, T)],
        pad_nrows: usize,
        pad_ncols: usize,
    ) -> Self {
        assert!(shape[0] <= pad_nrows);
        assert!(shape[1] <= pad_ncols);
        let mut buf = vec![T::zero(); pad_nrows * pad_ncols];
        triplets.iter().for_each(|&(i, k, val)| {
            buf[i * shape[1] + k] = val;
        });
        DenseMatrix {
            buf,
            nrows: shape[0],
            ncols: shape[1],
            pad_nrows,
            pad_ncols,
        }
    }
    /// Create a new [DenseMatrix] with rows permuted according to a permutation vector
    /// `sig_i`. This vector has the meaning `sig_i[new_i] = old_i`.
    /// # Example
    /// ```rust
    /// use spmmgen::DenseMatrix;
    ///
    /// let a = DenseMatrix {
    ///     buf: vec![
    ///         0.0, 1.0, 2.0,
    ///         3.0, 4.0, 5.0,
    ///         6.0, 7.0, 8.0,
    ///     ],
    ///     ncols:3,
    ///     nrows:3,
    ///     pad_ncols:3,
    ///     pad_nrows:3,
    /// };
    /// let sig_i = [2,0,1];
    /// let expected = DenseMatrix {
    ///     buf: vec![
    ///         6.0, 7.0, 8.0,
    ///         0.0, 1.0, 2.0,
    ///         3.0, 4.0, 5.0,
    ///     ],
    ///     ..a
    /// };
    /// assert_eq!(a.permute_rows(&sig_i), expected);
    /// ```
    pub fn permute_rows(&self, sig_i: &[usize]) -> DenseMatrix<T> {
        assert_eq!(
            self.pad_nrows,
            sig_i.len(),
            "Number of rows should be equal to len of sig_i."
        );
        let len_k = self.pad_ncols;

        let mut output = self.clone();
        for (new_i, old_i) in sig_i.iter().enumerate() {
            let idx_old = old_i * len_k;
            let idx_new = new_i * len_k;
            let right_new = &mut output.buf[idx_new..(idx_new + len_k)];
            let right_old = &self.buf[idx_old..(idx_old + len_k)];
            right_new.copy_from_slice(right_old);
        }
        output
    }
}
