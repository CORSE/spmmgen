use num_traits::Float;
use serde::{Deserialize, Serialize};
use std::error::Error;
use std::fmt::{Display, Write};
use std::str::FromStr;

/// Enum to select the targeted intrinsincs to use in generated code, using either AVX 2 or AVX 512.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize, Default)]
pub enum VectorInstructions {
    #[default]
    Avx2,
    Avx512,
}

#[derive(Debug, Clone, Copy)]
pub enum ConversionError {
    BadEnumVariant,
}

impl Display for ConversionError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{self:?}")
    }
}

impl Error for ConversionError {}

impl FromStr for VectorInstructions {
    type Err = ConversionError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "avx2" | "avx256" => Ok(Self::Avx2),
            "avx5" | "avx512" => Ok(Self::Avx512),
            _ => Err(ConversionError::BadEnumVariant),
        }
    }
}

impl Display for VectorInstructions {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let n = match self {
            Self::Avx2 => 2,
            Self::Avx512 => 512,
        };
        write!(f, "avx{n}")
    }
}

impl Display for MicroKernel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "UKernel i:{} j:{} {}", self.i, self.j, self.avx)
    }
}

impl VectorInstructions {
    /// Get the vector size in number of elements of type T.
    /// # Example
    /// ```rust
    /// use spmmgen::VectorInstructions::Avx2;
    /// assert_eq!(Avx2.vector_size::<f32>(), 8);
    /// assert_eq!(Avx2.vector_size::<f64>(), 4);
    /// ```
    pub fn vector_size<T>(self) -> usize {
        use VectorInstructions::*;
        let sizeof_t = std::mem::size_of::<T>();
        match self {
            Avx2 => 256 / 8 / sizeof_t,
            Avx512 => 512 / 8 / sizeof_t,
        }
    }

    /// Get the intrinsincs vector type to use in generated code.
    /// # Example
    /// ```rust
    /// use spmmgen::VectorInstructions::Avx2;
    /// assert_eq!(Avx2.vector_type::<f32>(), "__m256");
    /// assert_eq!(Avx2.vector_type::<f64>(), "__m256d");
    /// ```
    pub fn vector_type<T: Float>(self) -> &'static str {
        let nbytes = std::mem::size_of::<T>();
        use VectorInstructions::*;
        match (self, nbytes) {
            (Avx2, 4) => "__m256",
            (Avx2, 8) => "__m256d",
            (Avx512, 4) => "__m512",
            (Avx512, 8) => "__m512d",
            (avx, size) => panic!("Unexpected {avx:?} + float{}", 8 * size),
        }
    }

    /// Get the intrinsincs load to use in generated code.
    /// # Example
    /// ```rust
    /// use spmmgen::VectorInstructions::Avx2;
    /// assert_eq!(Avx2.load_vec::<f32>(), "_mm256_loadu_ps");
    /// assert_eq!(Avx2.load_vec::<f64>(), "_mm256_loadu_pd");
    /// ```
    pub fn load_vec<T: Float>(self) -> &'static str {
        let nbytes = std::mem::size_of::<T>();
        use VectorInstructions::*;
        match (self, nbytes) {
            (Avx2, 4) => "_mm256_loadu_ps",
            (Avx2, 8) => "_mm256_loadu_pd",
            (Avx512, 4) => "_mm512_loadu_ps",
            (Avx512, 8) => "_mm512_loadu_pd",
            (avx, size) => panic!("Unexpected {avx:?} + float{}", 8 * size),
        }
    }

    /// Get the intrinsincs broadcast (scalar expanded to vector)
    /// to use in generated code.
    /// # Example
    /// ```rust
    /// use spmmgen::VectorInstructions::Avx2;
    /// assert_eq!(Avx2.broadcast_scal2vec::<f32>(), "_mm256_set1_ps");
    /// assert_eq!(Avx2.broadcast_scal2vec::<f64>(), "_mm256_set1_pd");
    /// ```
    pub fn broadcast_scal2vec<T: Float>(self) -> &'static str {
        let nbytes = std::mem::size_of::<T>();
        use VectorInstructions::*;
        match (self, nbytes) {
            (Avx2, 4) => "_mm256_set1_ps",
            (Avx2, 8) => "_mm256_set1_pd",
            (Avx512, 4) => "_mm512_set1_ps",
            (Avx512, 8) => "_mm512_set1_pd",
            (avx, size) => panic!("Unexpected {avx:?} + float{}", 8 * size),
        }
    }

    /// Get the intrinsincs store to use in generated code.
    /// # Example
    /// ```rust
    /// use spmmgen::VectorInstructions::Avx2;
    /// assert_eq!(Avx2.store_vec::<f32>(), "_mm256_storeu_ps");
    /// assert_eq!(Avx2.store_vec::<f64>(), "_mm256_storeu_pd");
    /// ```
    pub fn store_vec<T: Float>(self) -> &'static str {
        let nbytes = std::mem::size_of::<T>();
        use VectorInstructions::*;
        match (self, nbytes) {
            (Avx2, 4) => "_mm256_storeu_ps",
            (Avx2, 8) => "_mm256_storeu_pd",
            (Avx512, 4) => "_mm512_storeu_ps",
            (Avx512, 8) => "_mm512_storeu_pd",
            (avx, size) => panic!("Unexpected {avx:?} + float{}", 8 * size),
        }
    }
    pub fn fma_vec<T: Float>(self) -> &'static str {
        let nbytes = std::mem::size_of::<T>();
        use VectorInstructions::*;
        match (self, nbytes) {
            (Avx2, 4) => "_mm256_fmadd_ps",
            (Avx2, 8) => "_mm256_fmadd_pd",
            (Avx512, 4) => "_mm512_fmadd_ps",
            (Avx512, 8) => "_mm512_fmadd_pd",
            (avx, size) => panic!("Unexpected {avx:?} + float{}", 8 * size),
        }
    }
}

/// Simple triplet used to summarize info for generating microkernel code.
#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub struct MicroKernel {
    /// Number of time to unroll on the I dimension.
    pub i: usize,

    /// Number of time to unroll on the J dimension.
    pub j: usize,

    /// What kind of intrinsincs should we use in generated code.
    pub avx: VectorInstructions,
}

impl Default for MicroKernel {
    /// Default is 6x2 AVX 2 (i.e the BLIS kernel).
    fn default() -> Self {
        MicroKernel {
            i: 6,
            j: 2,
            avx: VectorInstructions::Avx2,
        }
    }
}

const TABSIZE: usize = 2;

impl MicroKernel {
    /// Generated the code for loading one part of A in vector registers.
    /// It will generate `i` lines.
    /// # Arguments
    /// - `access_a`: access expression to A index.
    /// - `nest_level`: number of tabulation to insert.
    pub fn gen_loads_a<T: Float>(
        &self,
        access_a: &'static str,
        nest_level: usize,
    ) -> Result<String, Box<dyn Error>> {
        let spaces = " ".repeat(nest_level * TABSIZE);
        let load = self.avx.broadcast_scal2vec::<T>();
        let mut loads = String::new();
        for i in 0..self.i {
            let access = access_a.replace("unroll_i", &i.to_string());
            writeln!(loads, "{spaces}vecA_{i} = {load}(A[{access}]);")?;
        }
        Ok(loads)
    }

    /// Generated the code for loading one part of B in vector registers.
    /// It will generate `j` lines.
    /// # Arguments
    /// - `access_b`: access expression to B index.
    /// - `nest_level`: number of tabulation to insert.
    pub fn gen_loads_b<T: Float>(
        &self,
        access_b: &'static str,
        nest_level: usize,
    ) -> Result<String, Box<dyn Error>> {
        let spaces = " ".repeat(nest_level * TABSIZE);
        let load = self.avx.load_vec::<T>();
        let vector_size = self.avx.vector_size::<T>();
        let mut loads = String::new();
        for j in 0..self.j {
            let bj = j * vector_size;
            let access = access_b.replace("unroll_j", &bj.to_string());
            writeln!(loads, "{spaces}vecB_{j} = {load}(&B[{access}]);")?;
        }
        Ok(loads)
    }

    /// Generated the code for loading one part of C in vector registers.
    /// It will generate `i*j` lines.
    /// # Arguments
    /// - `access_c`: access expression to C index.
    /// - `nest_level`: number of tabulation to insert.
    pub fn gen_loads_c<T: Float>(
        &self,
        access_c: &'static str,
        nest_level: usize,
    ) -> Result<String, Box<dyn Error>> {
        let spaces = " ".repeat(nest_level * TABSIZE);
        let load = self.avx.load_vec::<T>();
        let vector_size = self.avx.vector_size::<T>();
        let mut loads = String::new();
        for i in 0..self.i {
            let access = access_c.replace("unroll_i", &i.to_string());
            for j in 0..self.j {
                let bj = j * vector_size;
                let access = access.replace("unroll_j", &bj.to_string());
                writeln!(loads, "{spaces}vecC_{i}_{j} = {load}(&C[{access}]);")?;
            }
        }
        Ok(loads)
    }

    /// Generated the code for storing vector register to one part of C.
    /// It will generate `i*j` lines.
    /// # Arguments
    /// - `access_c`: access expression to C index.
    /// - `nest_level`: number of tabulation to insert.
    pub fn gen_store_c<T: Float>(
        &self,
        access_c: &'static str,
        nest_level: usize,
    ) -> Result<String, Box<dyn Error>> {
        let spaces = " ".repeat(nest_level * TABSIZE);
        let store = self.avx.store_vec::<T>();
        let vector_size = self.avx.vector_size::<T>();
        let mut stores = String::new();
        for i in 0..self.i {
            let access = access_c.replace("unroll_i", &i.to_string());
            for j in 0..self.j {
                let bj = j * vector_size;
                let access = access.replace("unroll_j", &bj.to_string());
                writeln!(stores, "{spaces}{store}(&C[{access}], vecC_{i}_{j});")?;
            }
        }
        Ok(stores)
    }

    /// Generate the code for intrinsincs fused multiply add.
    /// It will generate `i*j` lines.
    /// # Arguments
    /// - `nest_level`: number of tabulation to insert.
    /// # Error
    /// Can return io error when writing to String, but it should never happen.
    pub fn gen_fma<T: Float>(&self, nest_level: usize) -> Result<String, Box<dyn Error>> {
        let spaces = " ".repeat(nest_level * TABSIZE);
        let fma = self.avx.fma_vec::<T>();
        let mut fmas = String::new();
        for i in 0..self.i {
            for j in 0..self.j {
                writeln!(
                    fmas,
                    "{spaces}vecC_{i}_{j} = {fma}(vecA_{i}, vecB_{j}, vecC_{i}_{j});"
                )?;
            }
        }
        Ok(fmas)
    }

    /// Generate the code for declaring vector to use in the kernel.
    /// It will generate `i + j + i*j` declarations.
    /// # Arguments
    /// - `nest_level`: number of tabulation to insert.
    /// # Error
    /// Can return io error when writing to String, but it should never happen.
    pub fn declare_vecs<T: Float>(&self, nest_level: usize) -> Result<String, Box<dyn Error>> {
        let mut res = " ".repeat(nest_level * TABSIZE);
        res.push_str(self.avx.vector_type::<T>());
        res.push(' ');
        for i in 0..self.i {
            write!(res, "vecA_{i}, ")?;
        }
        for j in 0..self.j {
            write!(res, "vecB_{j}, ")?;
        }
        for i in 0..self.i {
            for j in 0..self.j {
                write!(res, "vecC_{i}_{j}, ")?;
            }
        }
        res.pop().unwrap();
        res.pop().unwrap();
        res.push(';');
        res.push('\n');
        Ok(res)
    }

    /// All in one function for the main kernel part it, interleave the load and the fma.
    /// # Arguments
    /// - `nest_level`: number of tabulation to insert.
    /// - `access_a`: access expression to A index.
    /// - `access_b`: access expression to B index.
    /// # Error
    /// Can return io error when writing to String, but it should never happen.
    pub fn gen_code<T: Float>(
        &self,
        nest_level: usize,
        access_a: &'static str,
        access_b: &'static str,
    ) -> Result<String, Box<dyn Error>> {
        let spaces = " ".repeat(nest_level * TABSIZE);
        let vecsize = self.avx.vector_size::<T>();
        let fma = self.avx.fma_vec::<T>();
        let broadcast = self.avx.broadcast_scal2vec::<T>();
        let load = self.avx.load_vec::<T>();
        let mut res = String::new();
        for i in 0..self.i {
            for j in 0..self.j {
                if j == 0 {
                    let a = access_a.replace("unroll_i", &i.to_string());
                    writeln!(res, "{spaces}vecA_{i} = {broadcast}({a});")?;
                }
                if i == 0 {
                    let bj = j * vecsize;
                    let b = access_b.replace("unroll_j", &bj.to_string());
                    writeln!(res, "{spaces}vecB_{j} = {load}(&B[{b}]);")?;
                }
                writeln!(
                    res,
                    "{spaces}vecC_{i}_{j} = {fma}(vecA_{i}, vecB_{j}, vecC_{i}_{j});\n"
                )?;
            }
        }
        Ok(res)
    }

    pub fn tiling_j_loop(&self, tile_j: usize) -> (String, String) {
        if tile_j == 0 {
            return (String::new(), String::new());
        }
        let end = "  }\n".to_owned();
        let start = [
            "const int tile_size = J_TILE * ukern_j_size",
            "assert(J % tile_size == 0)",
            "for (jstart = 0; jstart < J; jstart += tile_size){",
            "  jend = jstart + tile_size;\n",
        ];

        (start.join(";\n  "), end)
    }
}
