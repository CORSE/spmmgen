use crate::microkernel::MicroKernel;
use crate::TemplateSpmm;
use libloading::Library;
use log::info;
use num_traits::Float;
use std::error::Error;
use std::fmt::Display;
use std::fs::File;
use std::io;
use std::io::Write;
use std::process::ExitStatus;
use std::{env::current_dir, path::PathBuf, process::Command};

/// This is the main entry point for generating code using the library.
/// You can change the compiler, and some file name as input/output.
///
/// ## Example
/// ```rust
/// use spmmgen::*;
/// // example with a 8x8 matrix
/// let n = 8;
/// let b:Vec<f32> =  (0..(n * n)).map(|i| i as f32).collect();
/// let mut c:Vec<f32> =  vec![0.0;n*n];
/// let triplets:[(usize,usize,f32);3] = [(1,1,1.0), (2,3,2.0), (4,5,3.0)];
/// let coo = Coo::new(&triplets, None);
/// let kernel = MicroKernel {i:1, j:1, avx:VectorInstructions::Avx2};
/// let lib = CodeGenerator::new()
///     .with_compiler("clang")
///     .gen_compile_load(&coo, kernel)
///     .unwrap();
/// unsafe {
///     coo.execute_gen_fn(
///         &lib,
///         n as i32,
///         b.as_ptr(),
///         c.as_mut_ptr()
///     );
/// }
/// ```
pub struct CodeGenerator {
    src_dir: PathBuf,
    c_compiler: String,
    src_filename: String,
    out_lib: String,
    debug: bool,
}

impl Default for CodeGenerator {
    fn default() -> Self {
        CodeGenerator {
            src_dir: current_dir().unwrap(),
            src_filename: "spmmgen.c".to_owned(),
            out_lib: "libspmmgenc.so".to_owned(),
            c_compiler: "gcc".to_owned(),
            debug: false,
        }
    }
}

impl CodeGenerator {
    /// Same as CodeGenerator::default
    /// # Defaults
    /// - Root directory for generated files `/tmp` ,can be changed with
    /// [with_root_dir](crate::CodeGenerator::with_root_dir)
    /// - ``
    /// i.e generate spmmgen.c source file and libspmmgenc.so library
    /// using gcc as C compiler (without debug flag -g).
    /// By default files are produced in /tmp
    pub fn new() -> Self {
        CodeGenerator::default()
    }

    /// Change the directory where source code and library will be produced.
    pub fn with_root_dir<P>(mut self, root: P) -> Self
    where
        PathBuf: From<P>,
    {
        self.src_dir = PathBuf::from(root);
        std::fs::create_dir_all(&self.src_dir).unwrap();
        self
    }

    /// Change the filename of the source code file that will be produced.
    pub fn with_src_filename(mut self, src: String) -> Self {
        self.src_filename = src;
        self
    }

    /// Change the C compiler to use.
    pub fn with_compiler<T: ToString>(mut self, compiler: T) -> Self {
        self.c_compiler = compiler.to_string();
        self
    }

    /// Change the name of the lib.so that will be produced.
    pub fn with_lib_filename(mut self, lib: String) -> Self {
        self.out_lib = lib;
        self
    }

    /// Change add the debug flag `-g` to the flags given to the C compiler.
    pub fn with_debug(mut self) -> Self {
        self.debug = true;
        self
    }

    fn create_src_file(&self, content: String) -> io::Result<()> {
        let path = self.src_dir.join(&self.src_filename);
        match std::fs::remove_file(&path) {
            Ok(_) => (),
            Err(e) if matches!(e.kind(), io::ErrorKind::NotFound) => (),
            other => {
                return other;
            }
        }
        let mut srcfile = File::create(path)?;
        srcfile.write_all(content.as_bytes())
    }

    fn compile_lib(&self) -> io::Result<ExitStatus> {
        info!("Compiling...");
        let mut flags = vec![
            "-shared",
            "-march=native",
            "-O2",
            &self.src_filename,
            "-o",
            &self.out_lib,
            "-fPIC",
        ];
        if self.debug {
            flags.push("-g");
        }
        Command::new(&self.c_compiler)
            .current_dir(&self.src_dir)
            .args(flags)
            .status()
    }

    /// Generate source code, Compile it using the given C compiler and load the produced shared
    /// library in a [Library] struct.
    pub fn gen_compile_load<F: Float + Display, T: TemplateSpmm<F>>(
        &self,
        spec: &T,
        ukern: MicroKernel,
    ) -> Result<Library, Box<dyn Error>> {
        let content = spec.generate_src(ukern)?;
        self.create_src_file(content)?;
        let status = self.compile_lib()?;
        assert!(status.success(), "Compilation failed");
        info!("Compilation succeed!");
        let path = self.src_dir.join(&self.out_lib);
        let lib = unsafe { Library::new(path)? };
        Ok(lib)
    }
}
